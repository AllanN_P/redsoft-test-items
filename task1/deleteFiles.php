<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//Функция получения всех используемых файлов в инфоблоке
function getIBFiles()
{
	global $DB;
	$dbFile = [];
	$results = $DB->Query('SELECT FILE_NAME, SUBDIR FROM b_file WHERE MODULE_ID = "iblock"');
	while ($row = $results->Fetch()) {
		$dbFile[] = 'upload/' . $row['SUBDIR'] . '/' . $row['FILE_NAME'];
	}
	return $dbFile;
}
//Рекурсивно выбираем файлы и проверяем на наличие связей с IB
function deleteUnrelatedFiles($dir, $dbFile)
{
	$files = scandir($_SERVER['DOCUMENT_ROOT'] . '/' . $dir);
	foreach ($files as $object) {
		if ($object == "." || $object == "..") {
			continue;
		}
		$path = $dir . '/' . $object;
		if (is_dir($_SERVER['DOCUMENT_ROOT'] . '/' . $path)) {
			deleteUnrelatedFiles($path, $dbFile);
		} else {
			if (!in_array($path, $dbFile)){
				if (unlink($_SERVER['DOCUMENT_ROOT'] . '/' . $dir . '/' . $object)) {
					echo "<p>Удален файл: $path</p>" ;
				}
			}
		}
  }

}
$dbFile = getIBFiles();
deleteUnrelatedFiles("upload/iblock", $dbFile);