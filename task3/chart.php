<?php
//Функция перебора столбцов
function eachRow($jsonArr)
{
	for ($rowIndex = 1, $colCount = count($jsonArr[0]), $countToNull = 0; $rowIndex < $colCount; $rowIndex++) {
		for ($colIndex = 0, $rowCount = count($jsonArr); $colIndex < $rowCount; $colIndex++) {
			if ($jsonArr[$colIndex][$rowIndex] == 100) {
				$countToNull++;
				if($countToNull > 3) {
					$jsonArr = rowToNull($jsonArr, $colIndex, $rowIndex, $countToNull);
				}
			} else {
				$countToNull = 0;
			}
		}
	}
	return $jsonArr;
}
//Функция зануления нужных значений
function rowToNull($arr, $colIndex, $rowIndex, $count) 
{
	for ($lastRow = $colIndex, $endIndex = $lastRow - $count; $lastRow > $endIndex; $lastRow--) {
		if($arr[$lastRow][$rowIndex] != null) {
			$arr[$lastRow][$rowIndex] =  null;
		} else {
			return $arr;
		}
	}
	return $arr;
}
function jsonChar($file, $resFileName = 'chart_result.json')
{
	//Получаем данные передаем на перебор
	$jsonArr = json_decode(file_get_contents($file), 1);
	$jsonRes = eachRow($jsonArr);
	//Записываем новые данные
	file_put_contents($resFileName, json_encode($jsonRes));
}
jsonChar('chart2.json');

