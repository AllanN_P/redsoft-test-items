<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('iblock');
CModule::IncludeModule('sale');
$IB_ID_CATALOG = 1;
$IB_ID_OFFERS = 2;
$productName = 'Тестовый товар';
$offerName = 'Тестовый ТП';
$ciBlockElement = new CIBlockElement;

$productId = $ciBlockElement->Add(
    array(
        'IBLOCK_ID' => $IB_ID_CATALOG,
        'NAME' => $productName,
        "ACTIVE" => "Y",
    )
);
if (!empty($ciBlockElement->LAST_ERROR)) {
    echo "Ошибка добавления товара: ". $ciBlockElement->LAST_ERROR;
    die();
}

CCatalogProduct::Add(
    array(
        "ID" => $productId,
        "TYPE" => \Bitrix\Catalog\ProductTable::TYPE_SKU
    )
);

$arLoadProductArray = array(
    "IBLOCK_ID"      => $IB_ID_OFFERS,
    "NAME"           => $offerName,
    "ACTIVE"         => "Y",
    'PROPERTY_VALUES' => array(
        'CML2_LINK' => $productId,
    )
);
$offerId = $ciBlockElement->Add($arLoadProductArray);
if (!empty($ciBlockElement->LAST_ERROR)) {
    echo "Ошибка добавления торгового предложения: ". $ciBlockElement->LAST_ERROR;
    die();
}
CCatalogProduct::Add(
    array(
        "ID" => $offerId,
        "TYPE" => \Bitrix\Catalog\ProductTable::TYPE_OFFER
    )
);